﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Reflection;
using System.Linq;
using System.Net.Mime;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace DataReadWriteApp
{
    /// <summary>
    /// Управление демонстрационными данными.
    /// </summary>
    public class DataManager
    {
        ListRand listRand;
        
        public ListRand ListRand => listRand;
        
        /// <summary>
        /// Заполняет список демонстрационными данными.
        /// </summary>
        public void FillData()
        {
            listRand = new ListRand();
            listRand.Add("Apple");
            listRand.Add("Lemon");
            listRand.Add("Pear");
            listRand.Add("Banana");
            listRand.Add("Orange");
            listRand.AddMany("Mango", "Lime", "Coconut", "Avocado", "Kiwi");
        }

        /// <summary>
        /// Вывод значений спика.
        /// </summary>
        public void Print()
        {
            foreach (var item in listRand)
            {
                Console.WriteLine("ListNode.Data: {0}\t\tListNode.Rand.Data: {1}, \tHashCode: {2}", item.Data, item.Rand.Data, item.GetHashCode());
            }
        }

        /// <summary>
        /// Возвращает путь.
        /// </summary>
        public string GetFilePath()
        {
            return Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + @"\testnode.dat";
        }

        public FileStream GetFileStream(FileMode fileMode)
        {
           return new FileStream(GetFilePath(), fileMode);
        }

        /// <summary>
        /// Сохраняет список в файл.
        /// </summary>
        public void Save()
        {
            listRand.Serialize(GetFileStream(FileMode.Create));  // выполняем сериализацию списка
        }

        /// <summary>
        /// Загружает список из файла.
        /// </summary>
        public void Load()
        {
            listRand = ListRand.Deserialize(GetFileStream(FileMode.Open)); // выполняем десериализацию списка
        }
    }


}

