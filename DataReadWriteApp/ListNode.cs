﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataReadWriteApp
{
    /// <summary>
    /// Узел двусвязанного списка.
    /// </summary>
    public class ListNode
    {
        public ListNode(string data) 
        {
            Data = data;
        }
        public ListNode Prev { get; set; } 
        public ListNode Next { get; set; }
        public ListNode Rand { get; set; } // произвольный элемент внутри списка
        public string Data { get; set; } // примем, что значение должно быть уникальным для списка

        public override int GetHashCode()
        {
            return EqualityComparer<string>.Default.GetHashCode(this.Data) ^ EqualityComparer<string>.Default.GetHashCode(this.Rand == null ? null : this.Rand.Data) ^ EqualityComparer<string>.Default.GetHashCode(this.Prev == null ? null : this.Prev.Data) ^ EqualityComparer<string>.Default.GetHashCode(this.Next == null ? null : this.Next.Data);
        }

        public override bool Equals(object obj)
        {
            if (obj.GetType() != this.GetType()) return false;

            ListNode l = (ListNode)obj;
           
            return (this.Data == l.Data && this.Rand == l.Rand && this.Prev == l.Prev && this.Next == l.Next);
            
        }
    }
}
