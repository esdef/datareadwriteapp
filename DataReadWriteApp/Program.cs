﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataReadWriteApp
{
    class Program
    {
        static void Main(string[] args)
        {
            DataManager dm = new DataManager(); // инициализиреум менеджер данных

            dm.FillData(); // заполняем список демонстрационными данными

            // выводим список в консоль
            Console.WriteLine("Создан двусвязный список:\n");
            dm.Print();

            // сохраняем
            dm.Save();
            Console.WriteLine("\n\n\nСписок сохранен в файл:\n{0}", dm.GetFilePath());

            dm.ListRand.Clear(); // очищаем список
            dm.Print(); // проверяем очищен ли список

            // читаем из файла и выводим в консоль
            dm.Load();
            Console.WriteLine("\n\n\nДвусвязный список восстановлен из файла:\n");
            dm.Print();
            Console.ReadLine();
        }
    }
}
