﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace DataReadWriteApp
{
    /// <summary>
    /// Двусвязанный список.
    /// </summary>
    public class ListRand : IEnumerable<ListNode>  // Реализуем интерфейс IEnumerable для вывода элементов списка
    {
        ListNode head; // первый элемент
        ListNode tail; // последний элемент
        int count;  // количество элементов в списке

        public ListNode Head => head; 
        public ListNode Tail => tail;
        public int Count => count;

        public ListRand(ListNode head = null, ListNode tail = null, int count = 0)
        {
            this.head = head;
            this.tail = tail;
            this.count = count;
        }

        /// <summary>
        /// Сериализация списка.
        /// </summary>
        public void Serialize(FileStream s)
        {
            ListNode current = head; 
            string data;

            using (BinaryWriter writer = new BinaryWriter(s)) 
            {
                // последовательно преобразуем значения списка и записываем в файл
                while (current != null)
                {
                    var r = this.Select((node, index) => new {node, index}).First(e => e.node.Equals(current.Rand));
                    data = String.Concat(current.Data, '|', r.index);
                    writer.Write(data);
                   
                    current = current.Next;
                }
                writer.Close();
            }
            s.Close();
        }

        /// <summary>
        /// Десериализация списка.
        /// </summary>
        public static ListRand Deserialize(FileStream s) 
        {
            ListRand result = new ListRand();
            List<string> data = new List<string>();

            using (BinaryReader reader = new BinaryReader(s)) 
            {
                // последовательно заполняем список
                while (reader.BaseStream.Position < reader.BaseStream.Length)
                {
                    data.Add(reader.ReadString());
                }
                result.AddManyFromFile(data.ToArray());

                reader.Close(); 

            }
            s.Close();

            return result;
        }

        /// <summary>
        /// Добавление узла в список
        /// </summary>
        private void AddNode(string data)
        {
            ListNode node = new ListNode(data);
            ListNode previous = tail;

            if (head == null)
                head = node;
            else
                tail.Next = node;
            tail = node;
            tail.Prev = head.Equals(tail) ? null : previous;
            count++;
        }

        /// <summary>
        /// Добавление одного узла в список
        /// </summary>
        public void Add(string data)
        {
            AddNode(data);

            // устанавливаем ссылку на случайный элемент списка
            tail.Rand = head.Equals(tail) ? head : RandomNode();
        }

        /// <summary>
        /// Добавление нескольких узлов в список
        /// </summary>
        public void AddMany(params string[] list)
        {
            int i = 0;

            foreach (var e in list)
            {
                AddNode(e);
            }

            ListNode current = tail;

            // выполняем обход в обратном порядке
            while (i < list.Count())
            {
                current.Rand = head.Equals(tail) ? head : RandomNode();
                current = current.Prev;
                i++;
            }
        }
        /// <summary>
        /// Добавление нескольких узлов в список при восстановлении из файла
        /// </summary>
        private void AddManyFromFile(string[] listData)
        {
            string[] data;
            foreach (var e in listData)
            {
                data = e.Split('|');
                AddNode(data[0]);
            }
            ListNode current = tail;
            
            foreach (var e in listData.Reverse())
            {
                data = e.Split('|');
                current.Rand = this.ElementAt(int.Parse(data[1]));
                current = current.Prev;
            }
        }
        /// <summary>
        /// Возврат элемента по значению Data. 
        /// </summary>
        public ListNode GetNodeByData(string data)
        {
            return this.Single(e => e.Data.Equals(data));
        }

        /// <summary>
        /// Возврат случайного элемента.
        /// </summary>
        public ListNode RandomNode()
        {
            int rndInt = Rnd.Next(0, count);
            return this.ElementAt(rndInt);
        }
        /// <summary>
        /// Очистка списка.
        /// </summary>
        public void Clear()
        {
            head = null;
            tail = null;
            count = 0;
        }

        // Инициализируем генератор
        private static readonly Random Rnd = new Random();
                
        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable)this).GetEnumerator();
        }

        IEnumerator<ListNode> IEnumerable<ListNode>.GetEnumerator()
        {
            ListNode current = head;
            while (current != null)
            {
                yield return current;
                current = current.Next;
            }
        }

    }
}
