﻿using System.IO;
using DataReadWriteApp;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DataReadWriteAppTest
{
    [TestClass]
    public class ListRandTests
    {
        private string GetFilePath()
        {
            return Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + @"\testnode.dat";
        }

        public FileStream GetFileStream(FileMode fileMode)
        {
            return new FileStream(GetFilePath(), fileMode);
        }

        public void Save(ListRand listRand)
        {
            listRand.Serialize(GetFileStream(FileMode.Create));
        }

        public ListRand Load()
        {
            var listRand = ListRand.Deserialize(GetFileStream(FileMode.Open)); 
            return listRand;
        }

        [TestMethod]
        public void TestListSerialization()
        {
            ListNode first = new ListNode("A");
            ListNode second = new ListNode("B");
            ListNode third = new ListNode("C");

            first.Next = second;
            second.Next = third;

            second.Prev = first;
            third.Prev = second;

            first.Rand = third;
            second.Rand = third;
            third.Rand = third;

            ListRand list = new ListRand(first, third, 3);
            Save(list);
            ListRand deserializedList = Load();

            list.Equals(deserializedList);
        }
    }
}
